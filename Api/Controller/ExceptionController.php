<?php

namespace Air\Core\Api\Controller;

use FOS\RestBundle\View\View;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Throwable;

use App\Core\Exception\ClientException;
use App\Core\Exception\Form\FormInvalidDataException;
use App\Core\Exception\ServerException;
use Air\Core\Api\Response\ErrorResponse;
use Air\Core\Api\Response\ErrorElement;
use Air\Core\Api\Service\Form\FormErrorListGeneratorInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;
use Air\Core\Service\Cache\CacheHelper;

class ExceptionController
{
    /** @var FormErrorListGeneratorInterface */
    private $formErrorListGenerator;

    /**
     * ExceptionController constructor.
     *
     * @param FormErrorListGeneratorInterface $formErrorListGenerator
     */
    public function __construct(FormErrorListGeneratorInterface $formErrorListGenerator)
    {
        $this->formErrorListGenerator = $formErrorListGenerator;
    }

    /**
     * @param Throwable $exception
     *
     * @return View
     */
    public function showException(Throwable $exception): View
    {
        $response = new ErrorResponse();

        $this->fillErrors($response, $exception);

        return View::create($response, $this->getCode($exception));
    }

    /**
     * @param ErrorResponse $response
     * @param Throwable     $exception
     */
    private function fillErrors(ErrorResponse $response, Throwable $exception): void
    {
        if ($exception instanceof FormInvalidDataException) {
            $this->formErrorListGenerator->generate($exception->getForm(), $response);
            return;
        }

        $error = new ErrorElement();
        $error->code = $this->getCode($exception);

        if ($exception instanceof ClientException || $exception instanceof ServerException
            || $exception instanceof HttpExceptionInterface) {
            $error->message = $exception->getMessage();
        } else {
            $error->message = 'Unexpected exception';
        }

        $response->errors[] = $error;
    }

    /**
     * @param Throwable $exception
     *
     * @return int
     */
    private function getCode(Throwable $exception): int
    {
        if ($exception instanceof ClientException || $exception instanceof ServerException) {
            return $exception->getCode();
        }

        if ($exception instanceof HttpExceptionInterface) {
            return $exception->getStatusCode();
        }

        return 500;
    }
}
