<?php

namespace Air\Core\Api\Controller;

use Air\Core\Api\Response\ErrorElement;
use Air\Core\Api\Response\ErrorResponse;
use Air\Core\Api\Service\ApiHelper;
use Air\Core\Api\Service\Form\EntityErrorListGenerator;
use Air\Core\Api\Service\Form\FormErrorListGenerator;
use Air\Core\Service\Serializer\SerializerHelper;
use Air\Core\Service\Cache\CacheHelper;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Cache\TagAwareCacheInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class BaseController extends AbstractController
{
    protected $entityErrorListGenerator;
    protected $formErrorListGenerator;

    protected $entityManager;
    protected $serializerHelper;

    public function __construct(
        FormErrorListGenerator   $formErrorListGenerator,
        EntityErrorListGenerator $entityErrorListGenerator,
        EntityManagerInterface   $entityManager,
        SerializerHelper         $serializerHelper
    )
    {
        $this->entityErrorListGenerator = $entityErrorListGenerator;
        $this->formErrorListGenerator = $formErrorListGenerator;
        $this->entityManager = $entityManager;
        $this->serializerHelper = $serializerHelper;
    }

    public function errorAnswer(array $errors, ?array $groups = [], int $code = Response::HTTP_BAD_REQUEST)
    {
        $context = new Context();
        if ($groups) {
            $context->setGroups($groups);
        }
        $errorResponse = new ErrorResponse();
        foreach ($errors as $error) {
            ;
            if (!($error instanceof ErrorElement)) {
                $error = new ErrorElement($error);
            }
            $errorResponse->errors[] = $error;
        }

        return View::create($errorResponse, $code)->setContext($context);
    }

    public function answer($response, ?array $groups = [], int $code = Response::HTTP_OK)
    {
        $context = new Context();
        if ($groups) {
            $context->setGroups($groups);
        }

        return View::create($response, $code)->setContext($context);
    }

    public function validateForms(array $forms): ?array
    {
        $response = new ErrorResponse();

        foreach ($forms as $form) {
            if ($form->isSubmitted() && !$form->isValid()) {
                $this->formErrorListGenerator->generate($form, $response);
            }
        }

        return $response->errors;
    }

    public function validateEntity(array $entities): ?array
    {
        $response = new ErrorResponse();

        foreach ($entities as $entity) {
            $this->entityErrorListGenerator->generate($entity, $response);
        }

        return $response->errors;
    }

    public function getErrorMessages(Form $form): array
    {
        return ApiHelper::getErrorMessages($form);
    }

    public function generateCacheId(Request $request, array $params = [])
    {
        return CacheHelper::generateCacheId($request);
    }

    public function jsonResponseFromArray(array $arrayData, ?string $cacheId = ""): ?JsonResponse
    {
        $data = $arrayData;
        $response = null;
        if ($data) {
            if (isset($data['fromCache'])) {
                $data['fromCache'] = true;
            }  

            $response = new JsonResponse($data, 200);
            $response->setEncodingOptions(JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        }
        return $response;
    }
}
