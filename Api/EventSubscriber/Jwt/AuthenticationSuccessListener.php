<?php

namespace Air\Core\Api\EventSubscriber\Jwt;

use JMS\Serializer\Serializer;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Application\Sonata\UserBundle\Model\UserInterface;
use App\User\Model\Security\SiginInData;
use App\User\Response\Security\SiginInResponse;
use Air\Core\Service\Serializer\SerializerHelper;

class AuthenticationSuccessListener
{
    protected int $tokenTtl;

    protected int $refreshTokenTtl;

    protected Serializer $serializer;

    protected SerializerHelper $serializerHelper;

    public function __construct($serializer, $tokenTtl, $refreshTokenTtl)
    {
        $this->serializer = $serializer;
        $this->refreshTokenTtl = $refreshTokenTtl;
        $this->tokenTtl = $tokenTtl;
        $this->serializerHelper = new SerializerHelper($serializer);
    }

    /**
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event): void
    {
        $data = $event->getData();
        $user = $event->getUser();

        if (!$user instanceof UserInterface) {
            return;
        }

        $siginInData = new SiginInData();

        $siginInData->token = $data['token'];
        $siginInData->refreshToken = $data['refreshToken'];
        $siginInData->tokenTtl = $this->tokenTtl;
        $siginInData->refreshTokenTtl = $this->refreshTokenTtl;
        $siginInData->user = $user;
        $siginInData->setTokenExpiredAt();
        $siginInData->setRefreshTokenExpiredAt();

        $event->setData($this->serializerHelper->toArray($siginInData, ['view', 'list']));
    }
}
