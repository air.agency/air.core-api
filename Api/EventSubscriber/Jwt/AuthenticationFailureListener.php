<?php

namespace Air\Core\Api\EventSubscriber\Jwt;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTInvalidEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTNotFoundEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTExpiredEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTFailureEventInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Air\Core\Api\Response\ErrorResponse;
use Air\Core\Api\Response\ErrorElement;

class AuthenticationFailureListener
{
    /**
     * @param AuthenticationFailureEvent $event
     */
    public function onAuthenticationFailureResponse(AuthenticationFailureEvent $event)
    {
        /* $response = new JWTAuthenticationFailureResponse("ggg"); */

        $data = new ErrorResponse();
        $data->errors = [
            new ErrorElement("Bad credentials, please verify that your username/password are correctly set")
        ];
        $response = new JsonResponse($data, JsonResponse::HTTP_UNAUTHORIZED, ['WWW-Authenticate' => 'Bearer']);
        $event->setResponse($response);
    }

    /**
     * @param JWTInvalidEvent $event
     */
    public function onJWTInvalid(JWTInvalidEvent $event)
    {
        $data = new ErrorResponse();
        $data->errors = [
            new ErrorElement("Token is invalid, please login again to get a new one")
        ];
        $response = new JsonResponse($data, JsonResponse::HTTP_FORBIDDEN, ['WWW-Authenticate' => 'Bearer']);
        $event->setResponse($response);
    }

    /**
     * @param JWTNotFoundEvent $event
     */
    public function onJWTNotFound(JWTNotFoundEvent $event)
    {
        $data = new ErrorResponse();
        $data->errors = [
            new ErrorElement("Missing token")
        ];
        $response = new JsonResponse($data, JsonResponse::HTTP_FORBIDDEN);
        $event->setResponse($response);
    }

    /**
     * @param JWTExpiredEvent $event
     */
    public function onJWTExpired(JWTExpiredEvent $event)
    {
        $data = new ErrorResponse();
        $data->errors = [
            new ErrorElement("Your token is expired, please renew it.")
        ];
        $response = new JsonResponse($data, JsonResponse::HTTP_UNAUTHORIZED);
        $event->setResponse($response);



        /* 
        $response = $event->getResponse();

        $response->setMessage('Your token is expired, please renew it.');
        */
    }
}
