<?php

namespace Air\Core\Api\EventSubscriber;

use Air\Core\Service\Cache\CacheHelper;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Events;
use Psr\Cache\InvalidArgumentException;
use ReflectionClass;
use ReflectionException;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

class CacheSubscriber implements EventSubscriber
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /** @var TagAwareCacheInterface */
    protected $apiRedisCache;

    /** @var array */
    protected $invalidateCacheClasses = [];

    /** @var array */
    protected $associations = [];

    public function __construct(
        EntityManager $entityManager,
        TagAwareCacheInterface $apiRedisCache
    )
    {
        $this->entityManager = $entityManager;
        $this->apiRedisCache = $apiRedisCache;
    }

    /**
     * @inheritdoc
     */
    public function getSubscribedEvents()
    {
        return [
            Events::loadClassMetadata,
            Events::postUpdate,
            Events::onFlush,
            Events::postFlush,
        ];
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $args)
    {
        $classMetadata = $args->getClassMetadata();

        $this->associations[$classMetadata->getName()] = [];

        foreach ($classMetadata->getAssociationMappings() as $associationMapping) {
            $this->associations[$classMetadata->getName()][] = $associationMapping['targetEntity'];
        }
    }

    /**
     * @param OnFlushEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        $entityManager = $args->getEntityManager();
        $unitOfWork = $entityManager->getUnitOfWork();
        foreach ($unitOfWork->getScheduledEntityInsertions() as $scheduledEntityInsertion) {
            $class = get_class($scheduledEntityInsertion);
            if (!in_array($class, $this->invalidateCacheClasses, true)) {
                $this->invalidateCacheClasses[] = $class;
            }
        }
        foreach ($unitOfWork->getScheduledEntityUpdates() as $scheduledEntityUpdate) {
            $class = get_class($scheduledEntityUpdate);
            if (!in_array($class, $this->invalidateCacheClasses, true)) {
                $this->invalidateCacheClasses[] = $class;
            }
        }
        foreach ($unitOfWork->getScheduledEntityDeletions() as $scheduledEntityDeletion) {
            $class = get_class($scheduledEntityDeletion);
            if (!in_array($class, $this->invalidateCacheClasses, true)) {
                $this->invalidateCacheClasses[] = $class;
            }
        }
    }

    /**
     * @param PostFlushEventArgs $args
     *
     * @throws InvalidArgumentException
     * @throws ReflectionException
     */
    public function postFlush(PostFlushEventArgs $args)
    {
        $tags = [];
        foreach ($this->invalidateCacheClasses as $invalidateCacheClass) {
            $tags[] = CacheHelper::getTagByClass($invalidateCacheClass);
            $tags = array_merge($tags, $this->getTagsForClassAssociations($invalidateCacheClass));

            $reflectionClass = new ReflectionClass($invalidateCacheClass);
            while ($parent = $reflectionClass->getParentClass()) {
                $tags[] = CacheHelper::getTagByClass($parent->getName());
                $tags = array_merge($tags, $this->getTagsForClassAssociations($parent->getName()));
                $reflectionClass = $parent;
            }
        }

        $this->apiRedisCache->invalidateTags($tags);

        $this->invalidateCacheClasses = [];
    }

    /**
     * @param LifecycleEventArgs $args
     *
     * @throws InvalidArgumentException
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->apiRedisCache->invalidateTags([CacheHelper::getTagByEntity($entity)]);
        $entity = $args->getEntity();

        $this->apiRedisCache->invalidateTags([CacheHelper::getTagByEntity($entity)]);
    }

    /**
     * @param string $className
     *
     * @return array
     */
    private function getTagsForClassAssociations(string $className): array
    {
        if (!array_key_exists($className, $this->associations)) {
            return [];
        }

        $tags = [];
        foreach ($this->associations[$className] as $association) {
            $tags[] = CacheHelper::getTagByClass($association);
        }
        return $tags;
    }
}
