<?php
namespace Air\Core\Api\Response;

use Doctrine\ORM\Tools\Pagination\Paginator;
use JMS\Serializer\Annotation as Serializer;
use Air\Core\Api\Model\Pagination;
use Pagerfanta\Pagerfanta;

class PaginationResponse
{
    /**
     * Current page
     *
     * @var int
     *
     * @Serializer\Type("int")
     * @Serializer\Groups({"list", "view"})
     */
    public $page;

    /**
     * Page limit
     * @var int
     *
     * @Serializer\Type("int")
     * @Serializer\Groups({"list", "view"})
     */
    public $limit;

    /**
     * Page count
     * @var int
     *
     * @Serializer\Type("int")
     * @Serializer\Groups({"list", "view"})
     */
    public $pageCount;

    /**
     * Items count in current page
     * @var int
     *
     * @Serializer\Type("int")
     * @Serializer\Groups({"list", "view"})
     */
    public $itemsCount;

    /**
     * Total items count
     * @var int
     *
     * @Serializer\Type("int")
     * @Serializer\Groups({"list", "view"})
     */
    public $totalItemsCount;

    public function __construct(?Pagination $pagination = null)
    {
        if ($pagination) {
            $this->page = $pagination->getPage();
            $this->limit = $pagination->getLimit();
        }
    }

    public function fillPagerfanta(Pagerfanta $results)
    {
        $this->pageCount = ceil($results->getNbResults() / $this->limit);
        $this->itemsCount = count($results->getCurrentPageResults());
        $this->totalItemsCount = $results->getNbResults();
    }

    public function fill(Paginator $dataPaginator)
    {
        $this->pageCount = ceil($dataPaginator->count() / $this->limit);
        $this->itemsCount = count($dataPaginator->getIterator());
        $this->totalItemsCount = $dataPaginator->count();
    }
}

