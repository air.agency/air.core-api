<?php

namespace Air\Core\Api\Response;

use JMS\Serializer\Annotation as Serializer;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Air\Core\Api\Response\PaginationResponse;
use Air\Core\Api\Model\Pagination;

abstract class AbstractListResponse extends AbstractResponse
{
    /**
     * Pagination
     *
     * @Serializer\Type("Air\Core\Api\Response\PaginationResponse")
     * @Serializer\Groups({"list"})
     */
    public $pagination;

    public function __construct(?Paginator $dataPaginator, ?Pagination $pagination)
    {
        $this->data = $dataPaginator->getIterator()->getArrayCopy();
        $this->pagination = new PaginationResponse($pagination);
        $this->pagination->fill($dataPaginator);
    }
}
