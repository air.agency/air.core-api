<?php

namespace Air\Core\Api\Response;

use JMS\Serializer\Annotation as Serializer;

class DebugElement
{
    /**
     * Value
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"view", "list"})
     */
    public $value;

    /**
     * Code
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"view", "list"})
     */
    public $code;

    public function __construct(?string $value = "", ?string $code = "")
    {
        $this->value = $value;
        $this->code = $code;
    }
}
