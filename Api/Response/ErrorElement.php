<?php

namespace Air\Core\Api\Response;

use JMS\Serializer\Annotation as Serializer;

class ErrorElement
{
    /**
     * Error message
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"view", "list"})
     */
    public $message;

    /**
     * Error code
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"view", "list"})
     */
    public $code;

    public function __construct(?string $message = "", ?string $code = "")
    {
        $this->message = $message;
        $this->code = $code;
    }
}
