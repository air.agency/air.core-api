<?php

namespace Air\Core\Api\Response;

use JMS\Serializer\Annotation as Serializer;
use Air\Core\Api\Response\DebugElement;
use Air\Core\Api\Response\ErrorElement;

abstract class AbstractResponse implements \JsonSerializable
{
    /**
     * @var mixed
     */
    public $data;

    /**
     * Errors
     *
     * @Serializer\Type("array<Air\Core\Api\Response\ErrorElement>")
     * @Serializer\Groups({"view", "list"})
     */
    public array $errors = [];

    /**
     * CacheId
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"view", "list"})
     */
    public ?string $cacheId = null;

    /**
     * From cache
     *
     * @Serializer\Type("boolean")
     * @Serializer\Groups({"view", "list"})
     */
    public bool $fromCache = false;

    /**
     * Debug
     *
     * @Serializer\Type("array<Air\Core\Api\Response\DebugElement>")
     * @Serializer\Groups({"view", "list"})
     */
    public array $debug = [];

    /**
     * Tags
     *
     * @Serializer\Type("array")
     * @Serializer\Groups({"view", "list"})
     */
    public array $tags = [];

    public function __construct($data = [], array $errors = [], ?string $cacheId = null)
    {
        $this->data = $data;
        $this->errors = $errors;
        $this->cacheId = $cacheId;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data): AbstractResponse
    {
        $this->data = $data;
        return $this;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function setErrors(array $errors): AbstractResponse
    {
        $this->errors = $errors;
        return $this;
    }

    public function getCacheId()
    {
        return $this->cacheId;
    }

    public function setCacheId(?string $cacheId): AbstractResponse
    {
        $this->cacheId = $cacheId;
        return $this;
    }

    public function getFromCache()
    {
        return $this->fromCache;
    }

    public function setFromCache(bool $fromCache): AbstractResponse
    {
        $this->fromCache = $fromCache;
        return $this;
    }

    public function getDebug()
    {
        return $this->debug;
    }

    public function setDebug(array $debug): AbstractResponse
    {
        $this->debug = $debug;
        return $this;
    }

    public function addCacheParams(string $cacheId, array $tags)
    {
        $this->setCacheId($cacheId);
        $resultTags = [];
        foreach($tags as $tag) {
            if (is_array($tag)) {
                foreach($tag as $subTag) {
                    $resultTags[] = $subTag;
                }
            } else {
                $resultTags[] = $tag;
            }
        }
        $this->setTags($resultTags);
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param array $tags
     * @return AbstractResponse
     */
    public function setTags(array $tags): AbstractResponse
    {
        $this->tags = $tags;
        return $this;
    }

    public function jsonSerialize(): mixed
    {
        return [
            'data' => $this->getData(),
            'errors' => $this->getErrors(),
            'cacheId' => $this->getCacheId(),
            'debug' => $this->getDebug(),
            'tags' => $this->getTags(),
        ];
    }
}
