<?php

namespace Air\Core\Api\Repository;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Air\Core\Api\Model\FilterInterface;
use Air\Core\Api\Model\Pagination;

interface FilterRepositoryInterface
{
    /**
     * @param FilterInterface $filter
     * @param Pagination $pagination
     * @param Sort $sort
     * @return Paginator
     */
    public function listPaginate(FilterInterface $filter, ?Pagination $pagination, $sort = null): Paginator;
}
