<?php

namespace Air\Core\Api\Model;

use Air\Core\Api\Model\SortItem;

class AbstractSort implements SortInterface
{
    public function getSortedItems():?array
    {
        $fields = get_object_vars($this);
        $sorted = [];
        foreach($fields as $fieldCode => $value) {
            if ($value && $value instanceOf SortItem) {
                $value->setFieldCode($fieldCode);
                $sorted[] = $value;
            }
        }

        usort($sorted, function($a, $b) {
            if ($a->getOrdering() == $b->getOrdering()) {
                return 0;
            }

            return ($a->getOrdering() > $b->getOrdering()) ? -1 : 1;
        });

        return $sorted;
    }

}
