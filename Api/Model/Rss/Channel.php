<?php

namespace Air\Core\Api\Model\Rss;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\XmlList;
class Channel
{
    /**
     * Title
     *
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"list"})
     */
    protected ?string $title = null;

    /**
     * Link
     *
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"list"})
     */
    protected ?string $link = null;

    /**
     * Description
     *
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"list"})
     */
    protected ?string $description = null;

    /**
     * Items
     *
     * @var array
     * @XmlList(inline = true, entry = "item")
     * @Serializer\Type("array<Air\Core\Api\Model\Rss\Item>")
     * @Serializer\Groups({"list"})
     */
    protected $items;

    public function getTitle():?string
    {
        return $this->title;
    }

    public function setTitle(?string $title)
    {
        $this->title = $title;
        return $this;
    }

    public function getLink():?string
    {
        return $this->link;
    }

    public function setLink(?string $link)
    {
        $this->link = $link;
        return $this;
    }

    public function getDescription():?string
    {
        return $this->description;
    }

    public function setDescription(?string $description)
    {
        $this->description = $description;
        return $this;
    }

    public function getItems()
    {
        return $this->items;
    }

    public function setItems($items)
    {
        $this->items = $items;
        return $this;
    }
}
