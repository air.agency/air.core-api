<?php

namespace Air\Core\Api\Model\Rss;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class Item
{
    /**
     * Title
     *
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"list"})
     */
    protected ?string $title = null;

    /**
     * Link
     *
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"list"})
     */
    protected ?string $link = null;

    /**
     * Description
     *
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"list"})
     */
    protected ?string $description = null;

    public function getTitle():?string
    {
        return $this->title;
    }

    public function setTitle(?string $title):self
    {
        $this->title = $title;
        return $this;
    }

    public function getLink():?string
    {
        return $this->link;
    }

    public function setLink(?string $link):self
    {
        $this->link = $link;
        return $this;
    }

    public function getDescription():?string
    {
        return $this->description;
    }

    public function setDescription(?string $description):self
    {
        $this->description = $description;
        return $this;
    }
}
