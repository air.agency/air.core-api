<?php
namespace Air\Core\Api\Model;

use Air\Core\Api\Model\FilterInterface;
use Air\Core\Api\Model\Traits;

class GroupListFilter implements FilterInterface
{
    use Traits\IdsTrait;
    use Traits\TitleTrait;
    use Traits\CodeTrait;
    use Traits\TimestampableTrait;
}
