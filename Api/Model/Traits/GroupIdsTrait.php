<?php

namespace Air\Core\Api\Model\Traits;

trait GroupIdsTrait
{
    /** @var array|null */
    protected $groupIds = [];

    /**
     * @return array|null
     */
    public function getGroupIds(): ?array
    {
        return $this->groupIds;
    }

    /**
     * @param array|null $groupIds
     *
     * @return self
     */
    public function setGroupIds(?array $groupIds): self
    {
        $this->groupIds = $groupIds;

        return $this;
    }
}
