<?php

namespace Air\Core\Api\Model\Traits;

trait TitleTrait
{
    /** @var string|null */
    protected $title = null;

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     *
     * @return self
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }
}
