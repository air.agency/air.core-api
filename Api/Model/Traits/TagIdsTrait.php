<?php

namespace Air\Core\Api\Model\Traits;

trait TagIdsTrait
{
    /** @var array|null */
    protected $tagIds = [];

    /**
     * @return array|null
     */
    public function getTagIds(): ?array
    {
        return $this->tagIds;
    }

    /**
     * @param array|null $tags
     *
     * @return self
     */
    public function setTagIds(?array $ids): self
    {
        $this->ids = $ids;

        return $this;
    }
}
