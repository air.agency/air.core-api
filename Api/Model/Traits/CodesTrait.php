<?php

namespace Air\Core\Api\Model\Traits;

trait CodesTrait
{
    /** @var array|null */
    protected $codes = [];

    /**
     * @return array|null
     */
    public function getCodes(): ?array
    {
        return $this->codes;
    }

    /**
     * @param array|null $codes
     *
     * @return self
     */
    public function setCodes(?array $codes): self
    {
        $this->codes = $codes;

        return $this;
    }
}
