<?php

namespace Air\Core\Api\Model\Traits;

trait CodeTrait
{
    /** @var string|null */
    protected $code = null;

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string|null $code
     *
     * @return self
     */
    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }
}
