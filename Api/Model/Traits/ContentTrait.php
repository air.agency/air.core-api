<?php

namespace Air\Core\Api\Model\Traits;

trait ContentTrait
{
    /** @var string|null */
    protected $content = null;

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     *
     * @return self
     */
    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }
}
