<?php

namespace Air\Core\Api\Model\Traits\Sort;

use Air\Core\Api\Model\SortItem;

trait CodeTrait
{
    /** @var SortItem|null */
    protected ?SortItem $code = null;

    /**
     * @return SortItem|null
     */
    public function getCode(): ?SortItem
    {
        return $this->code;
    }

    /**
     * @param SortItem|null $code
     *
     * @return self
     */
    public function setCode(?SortItem $code): self
    {
        $this->code = $code;

        return $this;
    }
}
