<?php

namespace Air\Core\Api\Model\Traits\Sort;

use Air\Core\Api\Model\SortItem;

trait TimestampableTrait
{
    /** @var SortItem|null */
    protected ?SortItem $createdAt = null;

    /** @var SortItem|null */
    protected ?SortItem $updatedAt = null;

    /**
     * @return SortItem|null
     */
    public function getCreatedAt(): ?SortItem
    {
        return $this->createdAt;
    }

    /**
     * @param SortItem|null $createdAt
     *
     * @return self
     */
    public function setCreatedAt(?SortItem $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return SortItem|null
     */
    public function getUpdatedAt(): ?SortItem
    {
        return $this->updatedAt;
    }

    /**
     * @param SortItem|null $updatedAt
     *
     * @return self
     */
    public function setUpdatedAt(?SortItem $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
