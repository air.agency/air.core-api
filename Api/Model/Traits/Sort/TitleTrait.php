<?php

namespace Air\Core\Api\Model\Traits\Sort;

use Air\Core\Api\Model\SortItem;

trait TitleTrait
{
    /** @var SortItem|null */
    protected ?SortItem $title = null;

    /**
     * @return string|null
     */
    public function getTitle(): ?SortItem
    {
        return $this->title;
    }

    /**
     * @param SortItem|null $title
     *
     * @return self
     */
    public function setTitle(?SortItem $title): self
    {
        $this->title = $title;

        return $this;
    }
}
