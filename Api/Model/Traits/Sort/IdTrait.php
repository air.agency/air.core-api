<?php

namespace Air\Core\Api\Model\Traits\Sort;

use Air\Core\Api\Model\SortItem;

trait IdTrait
{
    /** @var SortItem|null */
    protected ?SortItem $id = null;

    /**
     * @return SortItem|null
     */
    public function getId(): ?SortItem
    {
        return $this->id;
    }

    /**
     * @param SortItem|null $id
     *
     * @return self
     */
    public function setId(?SortItem $id): self
    {
        $this->id = $id;

        return $this;
    }
}
