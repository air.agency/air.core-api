<?php

namespace Air\Core\Api\Model\Traits;

trait IdsTrait
{
    /** @var array|null */
    protected $ids = [];

    /**
     * @return array|null
     */
    public function getIds(): ?array
    {
        return $this->ids;
    }

    /**
     * @param array|null $ids
     *
     * @return self
     */
    public function setIds(?array $ids): self
    {
        $this->ids = $ids;

        return $this;
    }
}
