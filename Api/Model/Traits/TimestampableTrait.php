<?php

namespace Air\Core\Api\Model\Traits;

use DateTime;

trait TimestampableTrait
{
    /** @var datetime|null */
    protected $createdAt = null;

    /** @var datetime|null */
    protected $createdAtFrom = null;

    /** @var datetime|null */
    protected $createdAtTo = null;

    /** @var datetime|null */
    protected $updatedAt = null;

    /** @var datetime|null */
    protected $updatedAtFrom = null;

    /** @var datetime|null */
    protected $updatedAtTo = null;

    /**
     * @return \DateTime
     */
    public function getCreatedAt():?DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return self
     */
    public function setCreatedAt(?DateTime $createdAt):self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAtFrom():?DateTime
    {
        return $this->createdAtFrom;
    }

    /**
     * @return self
     */
    public function setCreatedAtFrom(?DateTime $createdAtFrom):self
    {
        $this->createdAtFrom = $createdAtFrom;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAtTo():?DateTime
    {
        return $this->createdAtTo;
    }

    /**
     * @return self
     */
    public function setCreatedAtTo(?DateTime $createdAtTo):self
    {
        $this->createdAtTo = $createdAtTo;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt():?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @return self
     */
    public function setUpdatedAt(?DateTime $updatedAt):self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAtFrom():?DateTime
    {
        return $this->updatedAtFrom;
    }

    /**
     * @return self
     */
    public function setUpdatedAtFrom(?DateTime $updatedAtFrom):self
    {
        $this->updatedAtFrom = $updatedAtFrom;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAtTo():?DateTime
    {
        return $this->updatedAtTo;
    }

    /**
     * @return self
     */
    public function setUpdatedAtTo(?DateTime $updatedAtTo):self
    {
        $this->updatedAtTo = $updatedAtTo;
        return $this;
    }
}
