<?php

namespace Air\Core\Api\Model;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class SortItem
{
    const DIRECTIONS = ['asc', 'desc'];

    /**
     * Sort ordering
     *
     * @var string
     *
     * @Serializer\Type("integer")
     * @Serializer\Groups({"list"})
     *
     * @Assert\Length(min=1, max=999)
     */
    protected ?int $ordering = 1;

    /**
     * Sort direction
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"list"})
     *
     * @Assert\Choice(choices=SortItem::DIRECTIONS)
     */
    protected ?string $direction = null;

    private string $fieldCode = '';

    public function getFieldCode():string
    {
        return $this->fieldCode;
    }

    public function setFieldCode($fieldCode)
    {
        $this->fieldCode = $fieldCode;
        return $this;
    }

    public function getOrdering():int
    {
        return $this->ordering ? $this->ordering: 1;
    }

    public function setOrdering(?int $ordering):self
    {
        $this->ordering = $ordering;
        return $this;
    }

    public function getDirection():?string
    {
        return $this->direction;
    }

    public function setDirection(?string $direction):self
    {
        $this->direction = $direction;
        return $this;
    }
}
