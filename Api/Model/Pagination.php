<?php

namespace Air\Core\Api\Model;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class Pagination
{
    const LIMIT = [1, 2, 3, 10, 50, 100, 250, 500];

    const DEFAULT_PAGE = 1;

    const DEFAULT_LIMIT = 50;

    /**
     * Current page
     *
     * @var int
     *
     * @Serializer\Type("int")
     * @Serializer\Groups({"list"})
     * @Assert\Length(min=1, max=100)
     * @Assert\Positive
     */
    protected $page = self::DEFAULT_PAGE;

    /**
     * Page limit
     *
     * @var int
     *
     * @Serializer\Type("int")
     * @Serializer\Groups({"list"})
     * @Assert\Choice(choices=Pagination::LIMIT)
     */
    protected $limit = self::DEFAULT_LIMIT;

    public function getPage():?int
    {
        return $this->page ? $this->page: self::DEFAULT_PAGE;
    }

    public function setPage(?int $page):self
    {
        $this->page = $page;
        return $this;
    }

    public function getLimit():?int
    {
        return $this->limit ? $this->limit: self::DEFAULT_LIMIT;
    }

    public function setLimit(?int $limit):self
    {
        $this->limit = $limit;
        return $this;
    }
}
