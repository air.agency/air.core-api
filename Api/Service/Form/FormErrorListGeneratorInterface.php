<?php

namespace Air\Core\Api\Service\Form;

use Air\Core\Api\Response\ErrorResponse;
use Symfony\Component\Form\FormInterface;

interface FormErrorListGeneratorInterface
{
    /**
     * @param FormInterface $form
     * @param ErrorResponse $errorResponse
     */
    public function generate(FormInterface $form, ErrorResponse $errorResponse): void;
}
