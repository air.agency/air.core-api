<?php

namespace Air\Core\Api\Service\Form;

use Air\Core\Api\Response\ErrorResponse;
use Air\Core\Api\Response\ErrorElement;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class EntityErrorListGenerator
{
    protected $validator;

    public function __construct(ValidatorInterface $validator) {
        $this->validator = $validator;
    }

    /**
     * @inheritDoc
     */
    public function generate($entity, ErrorResponse $errorResponse): void
    {
        $this->generateEntityErrors($entity, $errorResponse);
    }

    /**
     * @param FormInterface $form
     * @param ErrorResponse $errorResponse
     * @param string        $basePath
     */
    private function generateEntityErrors($entity, ErrorResponse $errorResponse, string $basePath = ''): void
    {
        $entityErrors = $this->validator->validate($entity);

        foreach ($entityErrors as $entityError) {

            $error = new ErrorElement();
            $error->code = $entityError->getPropertyPath();
            $error->message = $entityError->getMessage();
            $errorResponse->errors[] = $error;
        }
    }
}
