<?php

namespace Air\Core\Api\Service\Form;

use Air\Core\Api\Response\ErrorResponse;
use Air\Core\Api\Response\ErrorElement;
use Symfony\Component\Form\FormInterface;

class FormErrorListGenerator implements FormErrorListGeneratorInterface
{
    /**
     * @inheritDoc
     */
    public function generate(FormInterface $form, ErrorResponse $errorResponse): void
    {
        $this->generateFormErrors($form, $errorResponse);
    }

    /**
     * @param FormInterface $form
     * @param ErrorResponse $errorResponse
     * @param string        $basePath
     */
    private function generateFormErrors(FormInterface $form, ErrorResponse $errorResponse, string $basePath = ''): void
    {
        if (!$basePath) {
            $basePath = $form->getName();
        }

        foreach ($form->getErrors() as $formError) {
            $params = $formError->getMessageParameters();
            $error = new ErrorElement();
            $error->code = $basePath;
            if (isset($params['{{ extra_fields }}'])) {
                $fields = $params['{{ extra_fields }}'];
                $fields = str_replace('"', "", $fields);
                $error->code = $basePath . ($basePath === '' ? '' : '.').$fields;
            }
            $error->message = $formError->getMessage();
            $errorResponse->errors[] = $error;
        }

        foreach ($form->all() as $formChild) {
            $childBasePath = $basePath . ($basePath === '' ? '' : '.') . $formChild->getName();
            $this->generateFormErrors($formChild, $errorResponse, $childBasePath);
        }
    }
}
