<?php

namespace Air\Core\Api\Service;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\Naming\SerializedNameAnnotationStrategy;
use \Symfony\Component\Form\Form;

class ApiHelper
{
    public static function serializeObject($object):?array
    {
        $serializer = SerializerBuilder::create()
            ->setPropertyNamingStrategy(new SerializedNameAnnotationStrategy(new IdenticalPropertyNamingStrategy()))
            ->build();

        $context = new SerializationContext();
        $context->setSerializeNull(true);

        return $serializer->toArray($object);
    }

    public static function getErrorMessages(Form $form): array
    {
        if (!$form->isSubmitted()) {
            return ['form' => 'Not submitted'];
        }

        $errors = [];

        foreach($form->getErrors() as $key => $error) {
            if ($form->isRoot()) {
                $errors['#'][] = $error->getMessage();
            } else {
                $errors[] = $error->getMessage();
            }
        }

        foreach($form->all() as $child) {
            if (!$child->isSubmitted()) {
                continue;
            }

            if (!$child->isValid()) {
                $errors[$child->getName()] = self::getErrorMessages($child);
            }
        }

        return $errors;
    }
}
