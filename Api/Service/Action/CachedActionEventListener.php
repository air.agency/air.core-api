<?php

namespace Air\Core\Api\Service\Action;

use Air\Core\Service\Serializer\SerializerHelper;
use Air\Core\Service\Cache\CacheHelper;
use Doctrine\Common\Annotations\Reader;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\View\View;
use Psr\Cache\CacheException;
use Psr\Cache\InvalidArgumentException;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\Cache\CacheItem;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

class CachedActionEventListener implements EventSubscriberInterface
{
    private Reader $reader;

    private TagAwareCacheInterface $cacheApi;

    private SerializerHelper $serializerHelper;

    private bool $cachedAction = false;

    private string $cacheId = '';

    private ?CacheItem $cacheResult;

    /**
     * @param Reader $reader
     * @param TagAwareCacheInterface $cacheApi
     */
    public function __construct(Reader $reader, TagAwareCacheInterface $cacheApi, SerializerHelper $serializerHelper)
    {
        $this->reader = $reader;
        $this->cacheApi = $cacheApi;
        $this->serializerHelper = $serializerHelper;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
            KernelEvents::VIEW => ['onKernelView', 100],
        ];
    }

    /**
     * @param            $response
     * @param array|null $groups
     *
     * @return View
     */
    public function answer($response, ?array $groups = []): View
    {
        $context = new Context();
        if ($groups) {
            $context->setGroups($groups);
        }

        return View::create($response, Response::HTTP_OK)->setContext($context);
    }

    /**
     * @throws ReflectionException
     */
    public function onKernelController(ControllerEvent $event): void
    {
        if ($event->getRequestType() !== HttpKernelInterface::MASTER_REQUEST) {
            return;
        }

        $controller = $event->getController();
        if (!is_array($controller)) {
            return;
        }

        $object = new ReflectionClass($controller[0]);
        $method = $object->getMethod($controller[1]);

        $methodAnnotations = $this->reader->getMethodAnnotations($method);
        foreach ($methodAnnotations as $methodAnnotation) {
            if ($methodAnnotation instanceof CachedAction) {
                $this->cachedAction = true;

                $this->cacheId = CacheHelper::generateCacheId($event->getRequest());
                $this->cacheResult = $this->cacheApi->getItem($this->cacheId);
                
                if ($this->cacheResult->isHit()) {
                    $response = $this->jsonResponseFromArray($this->cacheResult->get(), $this->cacheId);
                    if ($response !== null) {
                        $event->setController(static function () use ($response) {
                            return $response;
                        });
                    }
                }
            }
        }
    }

    /**
     * @param ViewEvent $event
     *
     * @throws CacheException
     * @throws InvalidArgumentException
     */
    public function onKernelView(ViewEvent $event): void
    {
        if (!$this->cachedAction) {
            return;
        }
        $result = $event->getControllerResult();
        if (!$result instanceof CachedResponse) {
            return;
        }

        $response = $result->getResponse();
        $response->addCacheParams($this->cacheId, $result->getTags());
        foreach ($result->getTags() as $tag) {
            $this->cacheResult->tag($tag);
        }

        $jsonData = $this->serializerHelper->toArray($response, $result->getGroups());
        $this->cacheResult->set($jsonData);

        $this->cacheApi->save($this->cacheResult);

        $event->setControllerResult($this->answer($response, $result->getGroups()));
    }

    /**
     * @param array $arrayData
     * @param string|null $cacheId
     *
     * @return JsonResponse|null
     */
    private function jsonResponseFromArray(array $arrayData, ?string $cacheId = ""): ?JsonResponse
    {
        $data = $arrayData;
        $response = null;
        if ($data) {
            if (isset($data['fromCache'])) {
                $data['fromCache'] = true;
            }
            
            $data['errors'] = [];
            $data['cacheId'] = $cacheId;
            $data['fromCache'] = true;
            $data['debug'] = false;

            $response = new JsonResponse($data, 200);
            $response->setEncodingOptions(JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        }

        return $response;
    }
}
