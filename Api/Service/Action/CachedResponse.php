<?php

namespace Air\Core\Api\Service\Action;

use Air\Core\Api\Response\AbstractResponse;

class CachedResponse
{
    /**
     * @var AbstractResponse
     */
    private AbstractResponse $response;

    /**
     * @var string[]
     */
    private array $tags;

    /**
     * @var string[]
     */
    private array $groups;

    /**
     * @param AbstractResponse $response
     * @param string[] $tags
     * @param string[] $groups
     */
    public function __construct(AbstractResponse $response, array $tags, array $groups)
    {
        $this->response = $response;
        $this->tags = $tags;
        $this->groups = $groups;
    }

    /**
     * @return AbstractResponse
     */
    public function getResponse(): AbstractResponse
    {
        return $this->response;
    }

    /**
     * @return string[]
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @return string[]
     */
    public function getGroups(): array
    {
        return $this->groups;
    }
}
