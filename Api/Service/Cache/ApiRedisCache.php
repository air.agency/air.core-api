<?php

namespace Air\Core\Api\Service\Cache;

use Predis\ClientInterface;
use Redis;
use RedisArray;
use RedisCluster;
use Symfony\Component\Cache\Adapter\RedisTagAwareAdapter;
use Symfony\Component\Cache\Marshaller\MarshallerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class ApiRedisCache extends RedisTagAwareAdapter
{
    protected $cacheNamespace = "";

    /** @var RequestStack */
    private $requestStack;

    /** @var bool|null */
    private $noCache;

    /**
     * @param Redis|RedisArray|RedisCluster|ClientInterface $redisClient The redis client
     * @param string                                        $namespace The default namespace
     * @param int                                           $defaultLifetime The default lifetime
     * @param MarshallerInterface|null                      $marshaller
     */
    public function __construct(
        $redisClient,
        string $namespace,
        int $defaultLifetime,
        bool $cacheEnabled,
        RequestStack $requestStack,
        MarshallerInterface $marshaller = null
    ) {
        $this->cacheNamespace = $namespace;

        $this->requestStack = $requestStack;

        if (!$cacheEnabled) {
            $this->noCache = true;
        }

        parent::__construct($redisClient, $namespace, $defaultLifetime, $marshaller);
    }

    /**
     * @inheritDoc
     */
    protected function doSave(array $values, int $lifetime, array $addTagData = [], array $delTagData = []): array
    {
        $jsonValues = [];
        $tagCode    = $this->cacheNamespace . ":\x00tags\x00";

        foreach ($values as $id => $value) {
            if (strpos($id, $tagCode) === false) {
                $jsonValues[$id] = json_encode($value, JSON_UNESCAPED_SLASHES);
            } else {
                $jsonValues[$id] = $value;
            }

            if (isset($value['tags']) && $value['tags'])
            {
                foreach($value['tags'] as $tag)
                {
                    $addTagData[$tag] = [$id];
                }
            }
        }

        return parent::doSave($jsonValues, $lifetime, $addTagData, $delTagData);
    }

    /**
     * @inheritDoc
     */
    protected function doFetch(array $ids): iterable
    {
        if ($this->getNoCache()) {
            return [];
        }

        $values     = parent::doFetch($ids);
        $jsonValues = [];
        foreach ($values as $id => $value) {
            if (strpos($id, "tags") === false) {
                $jsonValues[$id] = json_decode($value, true);
            } else {
                $jsonValues[$id] = $value;
            }
        }

        return $jsonValues;
    }

    /**
     * @return bool
     */
    private function getNoCache(): bool
    {


        if ($this->noCache !== null) {
            return $this->noCache;
        }

        $this->noCache = false;

        $request = $this->requestStack->getCurrentRequest();
        if ($request !== null) {
            $this->noCache = $request->headers->get('X-No-Cache') === 'true';
        }

        return $this->noCache;
    }

    /**
     * @inheritDoc
     */
    protected function doInvalidate(array $tagIds): bool
    {
        $tagsToInvalidate = [];
        $tagCode = $this->cacheNamespace . ":\x00tags\x00";
        foreach ($tagIds as $tagId) {
            if (str_starts_with($tagId, $tagCode)) {
                $tagsToInvalidate[] = str_replace($tagCode, '', $tagId);
            }
            else {
                $tagsToInvalidate[] = $tagId;
            }
        }

        return parent::doInvalidate($tagsToInvalidate);
    }
}
