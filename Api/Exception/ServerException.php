<?php

namespace Air\Core\Api\Exception;

use RuntimeException;
use Throwable;

class ServerException extends RuntimeException
{
    public function __construct($message = '', Throwable $previous = null)
    {
        parent::__construct($message, 500, $previous);
    }

}
