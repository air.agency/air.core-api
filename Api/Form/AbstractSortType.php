<?php

namespace Air\Core\Api\Form;

use Symfony\Component\Form\AbstractType;

class AbstractSortType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function getBlockPrefix(): string
    {
        return 'sort';
    }
}
