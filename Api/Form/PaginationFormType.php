<?php

namespace Air\Core\Api\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Air\Core\Api\Model\Pagination;

class PaginationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('page', NumberType::class, [
                'required' => false,
                'constraints' => [
                    new Assert\Positive()
                ],
                'empty_data' => (string) Pagination::DEFAULT_PAGE,
            ])
            ->add('limit', NumberType::class, [
                'required' => false,
                'constraints' => [
                    new Assert\Positive()
                ],
                'empty_data' => (string) Pagination::DEFAULT_LIMIT,
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class'    => Pagination::class,
            'method' => 'GET',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getBlockPrefix(): string
    {
        return 'pagination';
    }
}
