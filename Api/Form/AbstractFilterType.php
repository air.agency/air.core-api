<?php

namespace Air\Core\Api\Form;

use Symfony\Component\Form\AbstractType;

class AbstractFilterType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function getBlockPrefix(): string
    {
        return 'filter';
    }
}
