<?php

namespace Air\Core\Api\Form\Traits;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use DateTimeInterface;

trait TimestampableTrait
{
    /**
     * @params FormBuilderInterface $builder
     * @return FormBuilderInterface
     */
    public function addTimestamp(FormBuilderInterface $builder): FormBuilderInterface
    {
        $builder
            ->add('createdAt', DateTimeType::class, [
                'widget'   => 'single_text',
                'format'   => DateTimeInterface::ATOM,
                'required' => false
            ])
            ->add('createdAtFrom', DateTimeType::class, [
                'widget'   => 'single_text',
                'format'   => DateTimeInterface::ATOM,
                'required' => false
            ])
            ->add('createdAtTo', DateTimeType::class, [
                'widget'   => 'single_text',
                'format'   => DateTimeInterface::ATOM,
                'required' => false
            ])
            ->add('updatedAt', DateTimeType::class, [
                'widget'   => 'single_text',
                'format'   => DateTimeInterface::ATOM,
                'required' => false
            ])
            ->add('updatedAtFrom', DateTimeType::class, [
                'widget'   => 'single_text',
                'format'   => DateTimeInterface::ATOM,
                'required' => false
            ])
            ->add('updatedAtTo', DateTimeType::class, [
                'widget'   => 'single_text',
                'format'   => DateTimeInterface::ATOM,
                'required' => false
            ])
        ;

        return $builder;
    }

}
