<?php

namespace Air\Core\Api\Form\Traits;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

trait IdsTrait
{
    /**
     * @params FormBuilderInterface $builder
     * @return FormBuilderInterface
     */
    public function addIds(FormBuilderInterface $builder): FormBuilderInterface
    {
        $builder
            ->add('ids', CollectionType::class, [
                'entry_type' => NumberType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'required' => false
            ])
        ;

        return $builder;
    }

}
