<?php

namespace Air\Core\Api\Form\Traits;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

trait ContentTrait
{
    /**
     * @params FormBuilderInterface $builder
     * @return FormBuilderInterface
     */
    public function addContent(FormBuilderInterface $builder): FormBuilderInterface
    {
        $builder
            ->add('content', TextType::class, [
                'required' => false
            ])
        ;

        return $builder;
    }
}
