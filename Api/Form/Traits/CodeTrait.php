<?php

namespace Air\Core\Api\Form\Traits;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

trait CodeTrait
{
    /**
     * @params FormBuilderInterface $builder
     * @return FormBuilderInterface
     */
    public function addCode(FormBuilderInterface $builder): FormBuilderInterface
    {
        $builder
            ->add('code', TextType::class, [
                'required' => false
            ])
        ;

        return $builder;
    }

}
