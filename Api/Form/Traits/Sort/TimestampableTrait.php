<?php

namespace Air\Core\Api\Form\Traits\Sort;

use Symfony\Component\Form\FormBuilderInterface;
use Air\Core\Api\Form\SortItemFormType;

trait TimestampableTrait
{
    /**
     * @params FormBuilderInterface $builder
     * @return FormBuilderInterface
     */
    public function addTimestampableSort(FormBuilderInterface $builder): FormBuilderInterface
    {
        $builder
            ->add('createdAt', SortItemFormType::class, [
                'required' => false
            ])
            ->add('updatedAt', SortItemFormType::class, [
                'required' => false
            ])
        ;

        return $builder;
    }

}
