<?php

namespace Air\Core\Api\Form\Traits\Sort;

use Symfony\Component\Form\FormBuilderInterface;
use Air\Core\Api\Form\SortItemFormType;

trait CodeTrait
{
    /**
     * @params FormBuilderInterface $builder
     * @return FormBuilderInterface
     */
    public function addCodeSort(FormBuilderInterface $builder): FormBuilderInterface
    {
        $builder
            ->add('code', SortItemFormType::class, [
                'required' => false
            ])
        ;

        return $builder;
    }

}
