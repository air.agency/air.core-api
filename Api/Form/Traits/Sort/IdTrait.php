<?php

namespace Air\Core\Api\Form\Traits\Sort;

use Symfony\Component\Form\FormBuilderInterface;
use Air\Core\Api\Form\SortItemFormType;

trait IdTrait
{
    /**
     * @params FormBuilderInterface $builder
     * @return FormBuilderInterface
     */
    public function addIdSort(FormBuilderInterface $builder): FormBuilderInterface
    {
        $builder
            ->add('id', SortItemFormType::class, [
                'required' => false
            ])
        ;

        return $builder;
    }

}
