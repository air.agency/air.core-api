<?php

namespace Air\Core\Api\Form\Traits\Sort;

use Symfony\Component\Form\FormBuilderInterface;
use Air\Core\Api\Form\SortItemFormType;

trait TitleTrait
{
    /**
     * @params FormBuilderInterface $builder
     * @return FormBuilderInterface
     */
    public function addTitleSort(FormBuilderInterface $builder): FormBuilderInterface
    {
        $builder
            ->add('title', SortItemFormType::class, [
                'required' => false
            ])
        ;

        return $builder;
    }

}
