<?php

namespace Air\Core\Api\Form\Traits;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

trait TitleTrait
{
    /**
     * @params FormBuilderInterface $builder
     * @return FormBuilderInterface
     */
    public function addTitle(FormBuilderInterface $builder): FormBuilderInterface
    {
        $builder
            ->add('title', TextType::class, [
                'required' => false
            ])
        ;

        return $builder;
    }
}
