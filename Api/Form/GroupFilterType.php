<?php

namespace Air\Core\Api\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Air\Core\Api\Model\GroupListFilter;
use Air\Core\Api\Form\AbstractFilterType;
use Air\Core\Api\Form\Traits;

class GroupFilterType extends AbstractFilterType
{
    use Traits\TitleTrait;
    use Traits\CodeTrait;
    use Traits\IdsTrait;
    use Traits\TimestampableTrait;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder = $this->addIds($builder);
        $builder = $this->addTitle($builder);
        $builder = $this->addCode($builder);
        $builder = $this->addTimestamp($builder);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'method' => 'GET',
            'data_class'    => GroupListFilter::class,
        ]);
    }
}
